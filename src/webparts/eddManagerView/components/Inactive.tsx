import * as React from 'react';
import styles from './EddManagerView.module.scss';



export default class Inactive extends React.Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {value: ''};
      }

     private createItem = (items) => {
        let table = [];
    

        for (let i = 0; i < items.length; i++) {

            table.push(
            <tr>
                <td><a href={items[i].Link.Url}>{items[i].Title}</a></td>
                <td>{items[i].Manager.Title}</td>
                <td>{items[i].Line_x0020_Manager.Title}</td>
                <td>{items[i].Status}</td>
            </tr>
                );
        }
        return table;
      }
    

  public render(): React.ReactElement<any> {
    if(this.props.items.length){
    return (
      <div className={ styles.eddPersonalView }>
      <h2>Personal Yearly 1:1 Sessions</h2>
            <table>
                <tr>
                    <th>Title</th>
                    <th>Manager</th> 
                    <th>Local Manager</th> 
                    <th>Status</th>
                </tr>
                {this.createItem(this.props.items)}
                </table>
        </div>
    );
    } else {
        return (
            <div className={ styles.eddPersonalView }>
            <h2>Inactive Yearly 1:1 Sessions</h2>
            <h5>You have no inactive Yearly 1:1 sessions</h5>
            </div>
          );
    }
  }
}
