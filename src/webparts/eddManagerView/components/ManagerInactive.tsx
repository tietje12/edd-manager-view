import * as React from 'react';
import styles from './EddManagerView.module.scss';



export default class ManagerInactive extends React.Component<any, any> {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            isHidden: true,
        };
      }

      private toggleHidden () {
        this.setState({
          isHidden: !this.state.isHidden
        });
      }
     private createItem = (items) => {
        let table = [];
    

        for (let i = 0; i < items.length; i++) {

            table.push(
            <tr>
                <td><a href={items[i].Link.Url}>{items[i].Title}</a></td>
                <td>{items[i].Employee.Title}</td>
                <td>{items[i].Status}</td>
            </tr>
                );
        }
        return table;
      }
    

  public render(): React.ReactElement<any> {
    if(this.props.items.length){
    return (
      <div className={ styles.eddPersonalView }>
        <h2>Inactive managing Yearly 1:1 Sessions</h2>
        <button className={styles.button} onClick={this.toggleHidden.bind(this)} >
        {this.state.isHidden ? 'Show' : 'Hide'} inactive sessions
        </button>
        {!this.state.isHidden &&
            <table>
                <tr>
                    <th>Title</th>
                    <th>Employee</th> 
                    <th>Status</th>
                </tr>
                {this.createItem(this.props.items)}
                </table>
        }
        </div>
    );
    } else {
        return null;
    }
  }
}
