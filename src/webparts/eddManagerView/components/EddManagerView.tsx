import * as React from 'react';
import styles from './EddManagerView.module.scss';
import { IEddManagerViewProps } from './IEddManagerViewProps';
import Manager from './Manager';
import ManagerInactive from './ManagerInactive';


import {
  SPHttpClient,
  SPHttpClientResponse   
} from '@microsoft/sp-http';

// Declare interfaces
export interface ISPList {
  Title: string;
  Status: string;
  Id: string;
  Manager: {
    [Title: string]: {}
  };
  Employee: {
    [Title: string]: {}
  };
  Achievement: string;
  AchieveFollowUp: string;
  AchieveManager: string;
  Ambition: string;
  AmbitionManager: string;
  Curiosity: string;
  CuriosityManager: string;
  CurrentYearSuccess: string;
  DirectManagerComment: string;
  DoneBetter: string;
  LocalManagerComment: string;
  Opportunities: string;
  OtherInput: string;
  Respect: string;
  RespectManager: string;
  SmileAndJoy: string;
  SmileAndJoyManager: string;
  SuccessfulCooperation: string;
}

export interface State {
  items: any;
  users: any;
  selected:any;
  selectAll:any;
  payload:any;
  hideDialog: boolean;
  eddTitle: string;
  isLoaded: boolean;
  currentUserId: string;
  managerItems: any;
  managerInactiveItems: any;
  isManager: boolean;
 }

export default class EddManagerView extends React.Component<IEddManagerViewProps, State> {

  constructor(props) {
    super(props);
    this.state = {
      hideDialog: true,
      eddTitle: "",
      items: {
        value: []
      },
      users: {
        value: []
      },
      selected: {},
      payload: [], 
      managerItems: [], 
      managerInactiveItems: [], 
      selectAll: 0,
      isLoaded: false,
      currentUserId: "",
      isManager: false
    };
  }

    // All methods/functions inside componentDidMount will be executed after load
    public componentDidMount(): void {
   
      this._renderListAsync();
    
  
    }
    
    
    
    public _renderListAsync(): void {
      // Get current user from Sharepoint and add it to the state
      this._getCurrentUser()
       .then((res) => {

        this.setState({currentUserId: res.Id });
       });

       this._getListData()
       .then((res) => {
        this.setState({items:res});
        this.state.items.value.forEach(element => {
            if(element.Manager.Id == this.state.currentUserId || element.Line_x0020_Manager.Id == this.state.currentUserId  ){
              this.setState(prevState => ({
                managerItems: [...prevState.managerItems, element],
                isManager: true
              }));
            }
         });

  
       });


       this._getInactiveData()
       .then((res) => {
        this.setState({items:res});
        this.state.items.value.forEach(element => {

            if(element.Manager.Id == this.state.currentUserId || element.Line_x0020_Manager.Id == this.state.currentUserId  ){
              this.setState(prevState => ({
                managerInactiveItems: [...prevState.managerInactiveItems, element],
                isManager: true
              }));
            }
         });

       
       });

    }


  // Get current user
  private _getCurrentUser(): Promise<ISPList> {
    return this.props.context.spHttpClient.get(this.props.context.pageContext.web.absoluteUrl + `/_api/web/currentuser`, SPHttpClient.configurations.v1)
      .then((res: SPHttpClientResponse) => {

        return res.json();
        
      });
  }

// Get list items via Sharepoint Rest API
private _getListData(): Promise<ISPList> {
  return this.props.context.spHttpClient.get(this.props.context.pageContext.web.absoluteUrl + `/_api/web/lists/GetByTitle('Yearly1-1')/items()?$select=Link,Title,Status,Achievement,DoneBetter,CurrentYearSuccess,AchieveFollowUp,AchieveManager,SuccessfulCooperation,Respect,Ambition,Curiosity,SmileAndJoy,RespectManager,AmbitionManager,CuriosityManager,SmileAndJoyManager,Opportunities,OtherInput,DirectManagerComment,LocalManagerComment,Manager/Title,Manager/JobTitle,Line_x0020_Manager/EMail,Line_x0020_Manager/Title,Line_x0020_Manager/Id,Manager/EMail,Manager/Id,Employee/Title,Employee/Id,Employee/JobTitle, Employee/EMail&$expand=Manager,Employee, Line_x0020_Manager&$filter=Active eq 1&$top=1000`, SPHttpClient.configurations.v1)
    .then((response: SPHttpClientResponse) => {

      return response.json();
      
    });
}

// Get inactive items via Sharepoint Rest API
private _getInactiveData(): Promise<ISPList> {
  return this.props.context.spHttpClient.get(this.props.context.pageContext.web.absoluteUrl + `/_api/web/lists/GetByTitle('Yearly1-1')/items()?$select=Link,Title,Status,Achievement,DoneBetter,CurrentYearSuccess,AchieveFollowUp,AchieveManager,SuccessfulCooperation,Respect,Ambition,Curiosity,SmileAndJoy,RespectManager,AmbitionManager,CuriosityManager,SmileAndJoyManager,Opportunities,OtherInput,DirectManagerComment,LocalManagerComment,Manager/Title,Manager/JobTitle,Line_x0020_Manager/EMail,Line_x0020_Manager/Title,Line_x0020_Manager/Id,Manager/EMail,Manager/Id,Employee/Title,Employee/Id,Employee/JobTitle, Employee/EMail&$expand=Manager,Employee, Line_x0020_Manager&$filter=Active eq 0&$top=1000`, SPHttpClient.configurations.v1)
    .then((response: SPHttpClientResponse) => {

      return response.json();
      
    });
}

  public render(): React.ReactElement<IEddManagerViewProps> {
    if(this.state.isManager){
      return (
        <div className={ styles.eddPersonalView }>
          <Manager items={this.state.managerItems} />
          <ManagerInactive items={this.state.managerInactiveItems} />
  
        </div>
      );
    } else {
      return null;
    }
    
  }
}
