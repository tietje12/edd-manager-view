declare interface IEddManagerViewWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'EddManagerViewWebPartStrings' {
  const strings: IEddManagerViewWebPartStrings;
  export = strings;
}
